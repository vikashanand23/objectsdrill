function defaults(obj, defaultProps){
    for (let keyChild in defaultProps){
        let count = 0;
        for (let keyParent in obj){
            if (keyParent === keyChild){
                count += 1;
            }
        }
        if ( count === 0 ){
            obj[keyChild] = defaultProps[keyChild];
        }
    }
    return obj;
}

module.exports = defaults;
