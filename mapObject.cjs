function mapObject( obj, cb ){
    if (typeof obj === 'object'){
        for ( const key in obj ){
            cb( key, obj[key])
        }
    }
}

module.exports = mapObject;
