function keys(obj){
    const keysArray = [];
    if (typeof obj === 'object'){
        for ( const index in obj ){
            keysArray.push(index);
        }
    }
    return keysArray

}

module.exports = keys;