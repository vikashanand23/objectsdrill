function invert(obj){
    const result = {};
    if (typeof obj === 'object'){
        for (let key in obj){
            result[obj[key]] = key;
        }
    }
    return result;
}

module.exports = invert;
