// importing function
const mapObject = require('../mapObject.cjs');
const keys = require('../keys.cjs');
// defining an object
const testObject = {name: 'Bruce Wayne', age: 36, location: 'Gotham'};
const keysCount = (keys(testObject)).length;
//defining an empty object
const result = {};
let x = 1;
// invoking function and passing testObject as argument
if (keysCount>0){
    mapObject( testObject, function(key, value){
        result[key]=String(value).concat( '_',String(x) );
        x += 1;
    } );
}
//logging the result
console.log(result);
