// importing function
const values = require( '../values.cjs' );
// defining an object
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// invoking function and passing testObject as argument
const result = values( testObject );
//logging the result
console.log(result);
