// importing function
const pairs = require( '../pairs.cjs' );
// defining an object
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// invoking function and passing testObject as argument
const result = pairs( testObject );
//logging the result
console.log(result);
