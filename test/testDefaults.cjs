// importing function
const defaults = require( '../defaults.cjs' );
// defining an object
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const defaultProps = { name: 'Vikash', work: 'Batman'}
// invoking function and passing testObject as argument
const result = defaults( testObject, defaultProps );
//logging the result
console.log(result);
