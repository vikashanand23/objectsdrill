// importing function
const invert = require( '../invert.cjs' );
// defining an object
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// invoking function and passing testObject as argument
const result = invert( testObject );
//logging the result
console.log(result);
