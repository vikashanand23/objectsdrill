function pairs(obj){
    const result = [];
    if (typeof obj === 'object' ){
        for (let key in obj){
            result.push([key, obj[key]]);
        }
        
    }
    return result;
}

module.exports = pairs;
