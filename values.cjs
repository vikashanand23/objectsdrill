function values( obj ){
    const valueArray = [];
    if ( typeof obj === 'object' ){
        for (const key in obj){
            valueArray.push(obj[key]);
        }
    }
    return valueArray;
} 

module.exports = values;
